/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes/index.ts` as follows
|
| import './cart'
| import './customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.group(() => {
    Route.get('/', 'TasksController.index');
    Route.post('/tasks', 'TasksController.store');
    Route.patch('/tasks/:id', 'TasksController.update');
    Route.delete('/tasks/:id', 'TasksController.destroy');
}).middleware('auth');

Route.get('/register', 'AuthController.showRegister').middleware('guest');
Route.post('/register', 'AuthController.register');
Route.post('/logout', 'AuthController.logout');
Route.get('/login', 'AuthController.showLogin').middleware('guest');
Route.post('/login', 'AuthController.login');





























// ///////////////////////////////

// Route.on('/').render('welcome')
Route.get('/about/:name', async ({ params }) => {
    // Route.get('/', 'PagesController.home');
    return `This is the ${params.name}'s about page.`;
});

Route.get('/contact', async () => {
    return 'This is the contact page.';
});

// Optional params
Route.get('/car/:brand?', async ({ params }) => {
    return params.brand ? `This is ${params.brand}'s page` : 'This is car page';
});

Route.get('/school/:name?', 'PagesController.school');
Route.get('/payment/:brand?', 'PagesController.payment').as('payment');

Route.get('/dasbor', async ({ view }) => {
    return view.render('layouts.app');
});