import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class PagesController {
    public home({ view }: HttpContextContract) {
        return view.render('welcome');
    }

    public school({ params }: HttpContextContract) {
        return params.name ? `This is ${params.name} school` : 'This is school'; 
    }

    public payment({ view, params }: HttpContextContract) {
        const brand = params.brand;
        return view.render('payment', { brand });
    }
}
